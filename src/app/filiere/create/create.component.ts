import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

import { Filiere, model, module } from '../filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  // customMod = new model;

  filieres : Filiere[] =[] ;

   filiere: Filiere = {
     id:0,
     libelle:'',
     modules:[],
     stagiaires:[]
   }

  // filiere: any = {
  //   libelle:'',
  //   modules : this.module,
  //   stagiaires: this.satgiaire
  // }

  // filiereTitle : String = "FiliereTest";

  constructor(private filiereService: FiliereService) { 

    const source$ = this.filiereService.getFiliere();

    source$.subscribe((filiere: Filiere[])=> {
      this.filieres = filiere;   })

  }

  ngOnInit(): void {
  }

  // addModule(){
    
  // }

  handleFindAll(){
    
  }

  handleDelete(){
    this.filiereService.deleteFiliere(this.filiere).subscribe();
  }

  handleCreate(){
    console.log("Creation ...");

    if (this.filiere.id===0){
      this.filiereService.postFiliere(this.filiere).subscribe((res :HttpResponse<Filiere>) => {console.log(res)});
    } else {
      this.filiereService.putFiliere(this.filiere).subscribe((res :HttpResponse<Filiere>) => {console.log(res)});
    }

    // this.filiereService.putFiliere(this.filiere).subscribe((res :HttpResponse<Filiere>) => {console.log(res)});
  }



}
