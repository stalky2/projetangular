import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http'
import { Filiere, module } from './filiere.interface';
import { Observable } from 'rxjs';

const API = 'http://localhost:8150/api/filiere';

const API2 = 'http://localhost:8150/api/module';

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

  constructor(private http: HttpClient) { }

  postFiliere(filiere: Filiere): Observable<HttpResponse<Filiere>> {
    const headers: HttpHeaders = new HttpHeaders().set('Content-Type','application/json').set('X-Token', 'azerty123');

    return this.http.post<Filiere>(API, filiere, {headers, observe: 'response'});
  }

  putFiliere(filiere: Filiere): Observable<HttpResponse<Filiere>> {
    const headers: HttpHeaders = new HttpHeaders().set('Content-Type','application/json').set('X-Token', 'azerty123');

    return this.http.put<Filiere>(API, filiere, {headers, observe: 'response'});
  }

  deleteFiliere(filiere: Filiere) {
    const headers: HttpHeaders = new HttpHeaders().set('Content-Type','application/json').set('X-Token', 'azerty123');

    return this.http.delete<Filiere>(API, { body: filiere});
  }

  getFiliere() : Observable <Filiere[]>{ 
    return this.http.get<Filiere[]>(API);
}

  getModules() : Observable <module[]>{ 
    return this.http.get<module[]>(API2);
}



}
