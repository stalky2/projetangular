export interface Filiere {
    id?:number;
    libelle:string;
    modules:model[];
    stagiaires:stagiaire[];

}

export interface module{
    id?:number;
    dateDebut?:string;
    dateFin?:string;
    libelle?:string;
    filiereId?:number;
}

export interface stagiaire{
    "id":number;
}

export class model {
    "id": number;
}