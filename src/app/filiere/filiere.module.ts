import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { FiliereRoutingModule } from './filiere-routing.module';
import { CreateComponent } from './create/create.component';
import { FormsModule } from '@angular/forms';
import { ModelComponent } from './model/model.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    CreateComponent,
    ModelComponent
  ],
  imports: [
    CommonModule,
    FiliereRoutingModule,
    FormsModule,
    HttpClientModule
    
  ],
  exports: [CreateComponent]
})
export class FiliereModule { }
