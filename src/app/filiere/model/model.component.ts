import { Component, OnInit } from '@angular/core';
import { module } from '../filiere.interface';
import { FiliereService } from '../filiere.service';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {
  modules : module[] = [];

  constructor(private filiereService: FiliereService) { 

    const source$ = this.filiereService.getModules();

    source$.subscribe((module: module[])=> {
      this.modules = module; })

  }

 

  ngOnInit(): void {
  }

}
