import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { FiliereModule } from './filiere/filiere.module';

@NgModule({
  declarations: [
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    FiliereModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
